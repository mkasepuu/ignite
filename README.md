Ignite test assingment
===========

###BUILD

1. Setup config directory
    * Under ```src/main/resources``` you will find config.sample directory, that you need to setup for your workspace needs.
    * I like stateless configurations and cause I dont know what are your connection props, I decieded to add it to ```.gitignore```
    * Firstly, You can either:
        1. copy-paste  under  ```src/main/resources``` as ```src/main/resources/config```
        2. or you can set it up as JAVA_OPTS param as ```-Dconfig.location=$$YOUR_CONFIG_LOCATION$$```
    
2. Setup bower (https://bower.io/) and run:
    ``` bower install```
    * It will install necessary 3rd party dependencies for frontend development

5. Setup two databases. One for testing purposes and one for the application itself. Set it up in ```$config.dir$/application.properties```

4. Build it as a usual maven application (```mvn clean package/install```) and run builded ```.war``` file on a regular Servlet Container eg. Tomcat, Jetty