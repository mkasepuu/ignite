ALTER TABLE team_member DROP COLUMN firstname;
ALTER TABLE team_member DROP COLUMN lastname;
ALTER TABLE team_member DROP COLUMN age;
ALTER TABLE team_member add COLUMN name VARCHAR(50) NOT NULL;
