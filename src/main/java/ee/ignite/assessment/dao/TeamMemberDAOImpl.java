package ee.ignite.assessment.dao;

import ee.ignite.assessment.dao.base.BaseDAOImpl;
import ee.ignite.assessment.model.TeamMember;
import org.springframework.stereotype.Repository;

@Repository
public class TeamMemberDAOImpl extends BaseDAOImpl<TeamMember> implements TeamMemberDAO {

}
