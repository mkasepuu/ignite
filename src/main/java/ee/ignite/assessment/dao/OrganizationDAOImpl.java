package ee.ignite.assessment.dao;

import ee.ignite.assessment.dao.base.BaseDAOImpl;
import ee.ignite.assessment.model.Organization;
import org.springframework.stereotype.Repository;

@Repository
public class OrganizationDAOImpl extends BaseDAOImpl<Organization> implements OrganizationDAO {

}
