package ee.ignite.assessment.dao;

import ee.ignite.assessment.dao.base.BaseDAO;
import ee.ignite.assessment.model.Organization;

public interface OrganizationDAO extends BaseDAO<Organization> {

}
