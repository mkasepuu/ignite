package ee.ignite.assessment.dao.base;

import java.io.Serializable;
import java.util.List;

/**
 *
 * Created by mihkelk on 12.07.2017.
 */
public interface BaseDAO<T> {

    T get(Serializable id);

    T save(T bean);

    T saveOrUpdate(T bean);

    void delete(T bean);

    @SuppressWarnings("unchecked")
    List<T> getAll();

    T getInstanceIfExistsOrNew(Serializable id);

}
