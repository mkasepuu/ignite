package ee.ignite.assessment.dao.base;

import lombok.SneakyThrows;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 *
 * Created by mihkelk on 11.07.2017.
 */
public abstract class BaseDAOImpl<T> implements BaseDAO<T> {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @SneakyThrows(ClassNotFoundException.class)
    public Class<T> getGenericEntityClass() {
        ParameterizedType superClass = (ParameterizedType) getClass().getGenericSuperclass();
        String typeName = superClass.getActualTypeArguments()[0].getTypeName();
        return (Class<T>) Class.forName(typeName);
    }

    @SuppressWarnings("unchecked")
    public T get(Serializable id) {
        return entityManager.find(getGenericEntityClass(), id);
    }

    public T save(T bean) {
        entityManager.persist(bean);
        return bean;
    }

    public T saveOrUpdate(T bean) {
        entityManager.merge(bean);
        return bean;
    }

    public void delete(T bean) {
        entityManager.remove(bean);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        return entityManager.createQuery("from " + getGenericEntityClass().getName()).getResultList();
    }

    @Override
    public T getInstanceIfExistsOrNew(Serializable id) {
        try {
            if (id == null) {
                return getGenericEntityClass().newInstance();
            } else {
                return get(id);
            }
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException();
        }
    }
}
