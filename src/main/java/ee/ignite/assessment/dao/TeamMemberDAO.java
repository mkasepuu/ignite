package ee.ignite.assessment.dao;

import ee.ignite.assessment.dao.base.BaseDAO;
import ee.ignite.assessment.model.TeamMember;

public interface TeamMemberDAO extends BaseDAO<TeamMember> {

}
