package ee.ignite.assessment.dao;

import ee.ignite.assessment.dao.base.BaseDAOImpl;
import ee.ignite.assessment.model.Team;
import org.springframework.stereotype.Repository;

@Repository
public class TeamDAOImpl extends BaseDAOImpl<Team> implements TeamDAO {

}
