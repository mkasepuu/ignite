package ee.ignite.assessment.dao;

import ee.ignite.assessment.dao.base.BaseDAO;
import ee.ignite.assessment.model.Team;

public interface TeamDAO extends BaseDAO<Team> {

}
