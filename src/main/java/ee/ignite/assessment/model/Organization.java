package ee.ignite.assessment.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name="organization")
public class Organization {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String name;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "organization")
	private Set<Team> teams;

}
