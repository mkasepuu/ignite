package ee.ignite.assessment.util.notification;

import ee.ignite.assessment.util.message.StaticMessageSource;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * Created by mihkelk on 14.07.2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationMessage {

    public enum Type {

        ERROR("alert-danger"),
        SUCCESS("alert-success"),
        WARNING("alert-warning");

        Type(String label) {
            className = label;
        }

        String className;

        public String getClassName() {
            return className;
        }
    }

    private String codeOrText;
    private Type type;

    public static NotificationMessage error(String msg, Object... params) {
        return new NotificationMessage(StaticMessageSource.getMessage(msg, params), Type.ERROR);
    }

}
