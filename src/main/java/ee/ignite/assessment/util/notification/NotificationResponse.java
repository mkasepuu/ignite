package ee.ignite.assessment.util.notification;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by mihkelk on 14.07.2017.
 */
@Data
@EqualsAndHashCode(of = "messages")
public class NotificationResponse {


    private static final String ELEMENT_SAVED_MSG_ID = "global.element.saved";
    private static final String SUCCESS_MSG_ID = "global.success";

    private List<NotificationMessage> messages;

    public NotificationResponse() {
        messages = new ArrayList<>();
    }

    public NotificationResponse(NotificationMessage message) {
        messages = Collections.singletonList(message);
    }

    public static NotificationResponse elementSaved() {
        return getNotificationResponse(ELEMENT_SAVED_MSG_ID);
    }

    public static NotificationResponse success() {
        return getNotificationResponse(SUCCESS_MSG_ID);
    }

    private static NotificationResponse getNotificationResponse(String elementSavedMsgId) {
        NotificationMessage notificationMessage = new NotificationMessage();
        notificationMessage.setCodeOrText(elementSavedMsgId);
        notificationMessage.setType(NotificationMessage.Type.SUCCESS);
        return new NotificationResponse(notificationMessage);
    }

    public void newWarning(String msg) {
        messages.add(new NotificationMessage(msg, NotificationMessage.Type.WARNING));
    }

    public void newMessage(NotificationMessage msg) {
        messages.add(msg);
    }

    public void addError(String msg, Object... params) {
        newMessage(NotificationMessage.error(msg, params));
    }

    public void addEmptyFieldError(String fieldName) {
        addError("global.empty.field", fieldName);
    }

    public boolean hasErrors() {
        for (NotificationMessage message : messages) {
            if (message.getType() == NotificationMessage.Type.ERROR) {
                return true;
            }
        }
        return false;
    }

    public static void runValidation(Consumer<NotificationResponse> validation) throws NotificationException {
        NotificationResponse notifications = new NotificationResponse();
        validation.accept(notifications);
        if (notifications.hasErrors()) {
            throw new NotificationException(notifications);
        }
    }

}
