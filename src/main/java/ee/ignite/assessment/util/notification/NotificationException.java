package ee.ignite.assessment.util.notification;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * Created by mihkelk on 14.07.2017.
 */
@Data
@EqualsAndHashCode(of = "notificationResponse", callSuper = false)
public class NotificationException extends Exception {

    private NotificationResponse notificationResponse;

    public NotificationException(NotificationResponse notificationResponse) {
        this.notificationResponse = notificationResponse;
    }
    public NotificationException(NotificationMessage notificationMessage) {
        this.notificationResponse = new NotificationResponse(notificationMessage);
    }
}
