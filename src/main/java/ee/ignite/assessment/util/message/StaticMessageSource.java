package ee.ignite.assessment.util.message;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;


/**
 * @author https://github.com/chelu/jdal/blob/master/core/src/main/java/org/jdal/beans/StaticMessageSource.java
 */
@Slf4j
@Component
public class StaticMessageSource {

    private static MessageSource messageSource = null;

    @Autowired
	StaticMessageSource(MessageSource messageSource) {
		StaticMessageSource.messageSource = messageSource;
	}
	
	/**
	 * @return the messageSource
	 */
	public static MessageSource getMessageSource() {
		return messageSource;
	}
	
	/**
	 * @param messageSource the messageSource to set
	 */
	public static void setMessageSource(MessageSource messageSource) {
		StaticMessageSource.messageSource = messageSource;
	}
	
	/** 
	 * I18n Support
	 * @param code message code
	 * @return message or code if none defined
	 */
	public static String getMessage(String code) {
		try {
			return messageSource == null ?
				code : messageSource.getMessage(code, null, LocaleContextHolder.getLocale());
		}
		catch (Exception e) {
			return code;
		}
	}
	
	/** 
	 * I18n Support
	 * @param msr message source resolvable
	 * @return message or code if none defined
	 */
	 public static String getMessage(MessageSourceResolvable msr) {
		return messageSource == null ?
				msr.getDefaultMessage() : messageSource.getMessage(msr, LocaleContextHolder.getLocale());
	}
	 
	 public static String getMessage(String code, Object... args) {
		 return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
	 }
} 
