package ee.ignite.assessment.dto;

import ee.ignite.assessment.model.Organization;
import ee.ignite.assessment.model.Team;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class OrganizationDTO {

    private Integer id;

    private String name;

    private String teamNames;

    public OrganizationDTO(Organization organization) {
        id = organization.getId();
        name = organization.getName();
        Optional.ofNullable(organization.getTeams())
                .ifPresent(teams ->
                        teamNames = teams.stream()
                                .map(Team::getName)
                                .collect(Collectors.joining(", "))
                );
    }
}
