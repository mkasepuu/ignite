package ee.ignite.assessment.dto;

import ee.ignite.assessment.model.Team;
import ee.ignite.assessment.model.TeamMember;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * Created by mihkelk on 16.07.2017.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class TeamMemberDTO {

    private Integer id;

    private String name;

    private Set<Integer> teamIdList;

    private String teamNames;


    public TeamMemberDTO(TeamMember teamMember) {
        this.id = teamMember.getId();
        this.name = teamMember.getName();
        Optional.ofNullable(teamMember.getTeams()).ifPresent(this::manageTeams);
    }

    private void manageTeams(Set<Team> teamSet){
        this.teamIdList = teamSet.stream()
                .map(Team::getId)
                .collect(Collectors.toSet());
        this.teamNames = teamSet.stream()
                .map(Team::getName)
                .collect(Collectors.joining(", "));
    }

}
