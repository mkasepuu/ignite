package ee.ignite.assessment.dto;

import ee.ignite.assessment.model.Team;
import ee.ignite.assessment.model.TeamMember;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by mihkelk on 16.07.2017.
 */
@Data
@NoArgsConstructor
public class TeamDTO {

    private Integer id;
    private String name;
    private Integer rating;
    private OrganizationDTO organization;
    private String members;

    public TeamDTO(Team team) {
        id = team.getId();
        name = team.getName();
        rating = team.getRating();
        Optional.ofNullable(team.getOrganization()).ifPresent(org -> organization = new OrganizationDTO(org));
        Optional.ofNullable(team.getTeamMembers())
                .ifPresent(teamMemberSet -> members = teamMemberSet.stream()
                        .map(TeamMember::getName)
                        .collect(Collectors.joining(", "))
                );
    }
}
