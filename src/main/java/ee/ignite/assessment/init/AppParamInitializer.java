package ee.ignite.assessment.init;

import java.io.File;

/**
 * Created by mihkelk on 11.07.2017.
 */
public class AppParamInitializer {

    private static final String DEFAULT_CONFIG_LOCATION_PREFIX = "classpath:config";

    public static void initSystemParams() {

        if (System.getProperty("config.location") == null) {
            System.setProperty("config.location.prefix", DEFAULT_CONFIG_LOCATION_PREFIX);
            String autoLogConfPath = new File("src/main/resources/config/logback.xml").getAbsolutePath();
            System.setProperty("logback.configurationFile", autoLogConfPath);
        } else {
            String configLocationPrefix = "file:".concat(System.getProperty("config.location"));
            System.setProperty("config.location.prefix", configLocationPrefix);
            System.setProperty("logback.configurationFile", System.getProperty("config.location").concat("/logback.xml"));
        }
    }
}
