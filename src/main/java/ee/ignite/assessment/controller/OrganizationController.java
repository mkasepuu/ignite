package ee.ignite.assessment.controller;

import ee.ignite.assessment.dto.OrganizationDTO;
import ee.ignite.assessment.model.Organization;
import ee.ignite.assessment.service.OrganizationService;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.util.notification.NotificationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(value = "/organization")
public class OrganizationController {

    @Autowired
    private OrganizationService organizationService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView addOrganizationPage() {
        ModelAndView modelAndView = new ModelAndView("organization/add");
        modelAndView.addObject("organization", new Organization());
        return modelAndView;
    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public ModelAndView saveOrUpdateOrg(@ModelAttribute("organization") OrganizationDTO organization,
                                        RedirectAttributes attributes) {
        ModelAndView modelAndView;
        try {
            modelAndView = new ModelAndView(new RedirectView("/organization/list"));
            organizationService.saveOrUpdate(organization);
            attributes.addFlashAttribute("notificationResponse", NotificationResponse.elementSaved());
        } catch (NotificationException nf) {
            if (organization.getId() == null) {
                modelAndView = new ModelAndView(new RedirectView("/organization/add"));
            } else {
                modelAndView = new ModelAndView(new RedirectView("/organization/edit/" + organization.getId()));
            }
            attributes.addFlashAttribute("organization", organization);
            attributes.addFlashAttribute("notificationResponse", nf.getNotificationResponse());
        }
        return modelAndView;
    }

    @RequestMapping(value = "/list")
    public ModelAndView listOfOrganizations(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("organization/list");
        List<OrganizationDTO> organizations = organizationService.getOrganizations();
        modelAndView.addObject("organizations", organizations);
        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editOrganizationPage(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("organization/edit");
        OrganizationDTO organization = organizationService.getOrganization(id);
        modelAndView.addObject("organization", organization);
        return modelAndView;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteOrganization(@PathVariable Integer id, RedirectAttributes attributes) {
        ModelAndView modelAndView = new ModelAndView(new RedirectView("/organization/list"));
        organizationService.deleteOrganization(id);
        attributes.addFlashAttribute("notificationResponse", NotificationResponse.success());
        return modelAndView;
    }

}
