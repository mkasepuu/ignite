package ee.ignite.assessment.controller;

import ee.ignite.assessment.dto.TeamDTO;
import ee.ignite.assessment.model.Team;
import ee.ignite.assessment.service.OrganizationService;
import ee.ignite.assessment.service.TeamService;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.util.notification.NotificationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping(value = "/team")
public class TeamController {

    @Autowired
    private TeamService teamService;
    @Autowired
    private OrganizationService organizationService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView addTeamPage() {
        ModelAndView modelAndView = new ModelAndView("team/add");
        modelAndView.addObject("team", new Team());
        modelAndView.addObject("organizations", organizationService.getOrganizations());
        return modelAndView;
    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public ModelAndView saveOrUpdateTeam(@ModelAttribute("team") TeamDTO team,
                                         RedirectAttributes attributes) {
        ModelAndView modelAndView;
        try {
            modelAndView = new ModelAndView(new RedirectView("/team/list"));
            teamService.saveOrUpdate(team);
            attributes.addFlashAttribute("notificationResponse", NotificationResponse.elementSaved());
        } catch (NotificationException nf) {
            if (team.getId() == null) {
                modelAndView = new ModelAndView(new RedirectView("/team/add"));
            } else {
                modelAndView = new ModelAndView(new RedirectView("/team/edit/" + team.getId()));
            }
            attributes.addFlashAttribute("team", team);
            attributes.addFlashAttribute("notificationResponse", nf.getNotificationResponse());
        }
        return modelAndView;
    }

    @RequestMapping(value = "/list")
    public ModelAndView listOfTeams() {
        ModelAndView modelAndView = new ModelAndView("team/list");

        List<TeamDTO> teams = teamService.getTeams();
        modelAndView.addObject("teams", teams);

        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editTeamPage(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("team/edit");
        TeamDTO team = teamService.getTeam(id);
        modelAndView.addObject("team", team);
        modelAndView.addObject("organizations", organizationService.getOrganizations());
        return modelAndView;
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteTeam(@PathVariable Integer id, RedirectAttributes attributes) {
        ModelAndView modelAndView = new ModelAndView(new RedirectView("/team/list"));
        teamService.deleteTeam(id);
        attributes.addFlashAttribute("notificationResponse", NotificationResponse.success());
        return modelAndView;
    }
}
