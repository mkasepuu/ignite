package ee.ignite.assessment.controller;

import ee.ignite.assessment.dto.TeamMemberDTO;
import ee.ignite.assessment.service.TeamMemberService;
import ee.ignite.assessment.service.TeamService;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.util.notification.NotificationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping(value = "/team-member")
public class TeamMemberController {

    @Autowired
    private TeamMemberService teamMemberService;

    @Autowired
    private TeamService teamService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView addTeamMemberDTOPage() {
        ModelAndView modelAndView = new ModelAndView("team-member/add");
        modelAndView.addObject("teamMember", new TeamMemberDTO());
        modelAndView.addObject("teams", teamService.getTeams());
        return modelAndView;
    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public ModelAndView saveOrUpdateTeamMemberDTO(@ModelAttribute("teamMember") TeamMemberDTO teamMember,
                                               RedirectAttributes attributes) {
        ModelAndView modelAndView;
        try {
            modelAndView = new ModelAndView(new RedirectView("/team-member/list"));
            teamMemberService.saveOrUpdate(teamMember);
            attributes.addFlashAttribute("notificationResponse", NotificationResponse.elementSaved());
        } catch (NotificationException nf) {
            if (teamMember.getId() == null) {
                modelAndView = new ModelAndView(new RedirectView("/team-member/add"));
            } else {
                modelAndView = new ModelAndView(new RedirectView("/team-member/edit/" + teamMember.getId()));
            }
            attributes.addFlashAttribute("teamMember", teamMember);
            attributes.addFlashAttribute("notificationResponse", nf.getNotificationResponse());
        }
        return modelAndView;
    }

    @RequestMapping(value = "/list")
    public ModelAndView listOfTeamMemberDTOs() {
        ModelAndView modelAndView = new ModelAndView("team-member/list");

        List<TeamMemberDTO> teamMembers = teamMemberService.getTeamMembers();
        modelAndView.addObject("teamMembers", teamMembers);

        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editTeamMemberDTOPage(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("team-member/edit");
        TeamMemberDTO teamMember = teamMemberService.getTeamMember(id);
        modelAndView.addObject("teamMember", teamMember);
        modelAndView.addObject("teams", teamService.getTeams());
        return modelAndView;
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteTeamMemberDTO(@PathVariable Integer id, RedirectAttributes attributes) {
        ModelAndView modelAndView = new ModelAndView(new RedirectView("/team-member/list"));
        teamMemberService.deleteTeamMember(id);
        attributes.addFlashAttribute("notificationResponse", NotificationResponse.success());
        return modelAndView;
    }
}
