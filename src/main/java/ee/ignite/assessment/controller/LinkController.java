package ee.ignite.assessment.controller;

import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.util.notification.NotificationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Slf4j
@Controller
public class LinkController {

	@RequestMapping(value="/")
	public ModelAndView mainPage(RedirectAttributes redir) throws NotificationException {
		ModelAndView home = new ModelAndView("home");
		NotificationResponse notificationResponse = new NotificationResponse();
		notificationResponse.newWarning("home.welcome");
		home.addObject("notificationResponse", notificationResponse);
		return home;
	}

}
