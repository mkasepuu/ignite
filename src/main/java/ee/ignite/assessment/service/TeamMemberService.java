package ee.ignite.assessment.service;

import ee.ignite.assessment.dto.TeamMemberDTO;
import ee.ignite.assessment.model.Team;
import ee.ignite.assessment.model.TeamMember;
import ee.ignite.assessment.util.notification.NotificationException;

import java.util.List;

public interface TeamMemberService {

	TeamMember saveOrUpdate(TeamMemberDTO teamMember) throws NotificationException;

	TeamMemberDTO getTeamMember(Integer id);

	void deleteTeamMember(Integer id);

	List<TeamMemberDTO> getTeamMembers();

    void deleteTeamMemberTeamConnection(TeamMember teamMember, Team team);
}
