package ee.ignite.assessment.service;

import ee.ignite.assessment.dao.TeamDAO;
import ee.ignite.assessment.dao.TeamMemberDAO;
import ee.ignite.assessment.dto.TeamMemberDTO;
import ee.ignite.assessment.model.Team;
import ee.ignite.assessment.model.TeamMember;
import ee.ignite.assessment.util.message.StaticMessageSource;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.util.notification.NotificationResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TeamMemberServiceImpl implements TeamMemberService  {

    private final TeamMemberDAO teamMemberDAO;
    private final TeamDAO teamDAO;

    @Autowired
    public TeamMemberServiceImpl(TeamMemberDAO teamMemberDAO, TeamDAO teamDAO) {
        this.teamMemberDAO = teamMemberDAO;
        this.teamDAO = teamDAO;
    }

    @Transactional
    public TeamMember saveOrUpdate(TeamMemberDTO dto) throws NotificationException {
        validate(dto);
        TeamMember teamMember = teamMemberDAO.getInstanceIfExistsOrNew(dto.getId());
        teamMember.setName(dto.getName());
        saveOrUpdateTeamConnections(dto, teamMember);
        return teamMemberDAO.saveOrUpdate(teamMember);
    }

    @Transactional
    public TeamMemberDTO getTeamMember(Integer id) {
        TeamMember teamMember = teamMemberDAO.get(id);
        if (teamMember == null) {
            throw new UnsupportedOperationException("Nonexistent team member");
        }
        return new TeamMemberDTO(teamMember);
    }

    @Transactional
    public void deleteTeamMemberTeamConnection(TeamMember teamMember, Team teamBean) {
        teamMember.setTeams(
                teamMember.getTeams().stream().filter(team -> !team.equals(teamBean)).collect(Collectors.toSet())
        );
        teamMemberDAO.saveOrUpdate(teamMember);
    }

    @Transactional
    public void deleteTeamMember(Integer id) {
        TeamMember teamMember = teamMemberDAO.get(id);
        if (teamMember == null) {
            throw new UnsupportedOperationException("Nonexistent entity");
        }
        teamMember.getTeams().clear();
        teamMemberDAO.delete(teamMember);
    }

    @Transactional
    public List<TeamMemberDTO> getTeamMembers() {
        return teamMemberDAO.getAll().stream().map(TeamMemberDTO::new).collect(Collectors.toList());
    }

    private void validate(TeamMemberDTO teamMember) throws NotificationException {
        NotificationResponse.runValidation(notificationResponse -> {
            if (StringUtils.isBlank(teamMember.getName())) {
                notificationResponse.addEmptyFieldError(StaticMessageSource.getMessage("teamMember.name"));
            }
        });
    }

    private void saveOrUpdateTeamConnections(TeamMemberDTO dto, TeamMember teamMember) {
        Set<Team> teamSet = new HashSet<>();
        dto.getTeamIdList().forEach(id -> {
            Team existingTeam = teamDAO.get(id);
            if (existingTeam == null) {
                throw new UnsupportedOperationException("Nonexistent entity");
            }
            teamSet.add(existingTeam);
        });
        teamMember.setTeams(teamSet);
    }
}
