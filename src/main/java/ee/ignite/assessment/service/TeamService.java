package ee.ignite.assessment.service;

import ee.ignite.assessment.dto.TeamDTO;
import ee.ignite.assessment.model.Team;
import ee.ignite.assessment.util.notification.NotificationException;

import java.util.List;

public interface TeamService {

	Team saveOrUpdate(TeamDTO team) throws NotificationException;

	TeamDTO getTeam(Integer id);

	void deleteTeam(Integer id);

	void deleteOrganizationConnection(Team bean);

	List<TeamDTO> getTeams();

}
