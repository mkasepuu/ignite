package ee.ignite.assessment.service;

import ee.ignite.assessment.dto.OrganizationDTO;
import ee.ignite.assessment.model.Organization;
import ee.ignite.assessment.util.notification.NotificationException;

import java.util.List;

/**
 *
 * Created by mihkelk on 12.07.2017.
 */
public interface OrganizationService {

    Organization saveOrUpdate(OrganizationDTO organization) throws NotificationException;

    OrganizationDTO getOrganization(Integer id);

    void deleteOrganization(Integer id);

    List<OrganizationDTO> getOrganizations();

}
