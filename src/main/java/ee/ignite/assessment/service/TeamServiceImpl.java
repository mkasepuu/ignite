package ee.ignite.assessment.service;

import ee.ignite.assessment.dao.OrganizationDAO;
import ee.ignite.assessment.dao.TeamDAO;
import ee.ignite.assessment.dto.TeamDTO;
import ee.ignite.assessment.util.message.StaticMessageSource;
import ee.ignite.assessment.model.Organization;
import ee.ignite.assessment.model.Team;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.util.notification.NotificationResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeamServiceImpl implements TeamService {
	
	private TeamDAO teamDAO;

	private OrganizationDAO organizationDAO;

	private TeamMemberService teamMemberService;

	@Autowired
	public TeamServiceImpl(TeamDAO teamDAO, OrganizationDAO organizationDAO, TeamMemberService teamMemberService) {
		this.teamDAO = teamDAO;
		this.organizationDAO = organizationDAO;
		this.teamMemberService = teamMemberService;
	}


	@Transactional
	public Team saveOrUpdate(TeamDTO dto) throws NotificationException {
		validate(dto);
		Team team = teamDAO.getInstanceIfExistsOrNew(dto.getId());
		team.setName(dto.getName());
		team.setRating(dto.getRating());
		if (dto.getOrganization() != null && dto.getOrganization().getId() != null) {
			Organization organization = organizationDAO.get(dto.getOrganization().getId());
			if (organization == null) {
				throw new UnsupportedOperationException("Nonexistent organisation");
			}
			team.setOrganization(organization);
		}
		return teamDAO.saveOrUpdate(team);
	}

	@Transactional
	public TeamDTO getTeam(Integer id) {
		Team team = teamDAO.get(id);
		if (team == null) {
			throw new UnsupportedOperationException("Nonexistent team");
		}
		return new TeamDTO(team);
	}


	@Transactional
	public void deleteOrganizationConnection(Team bean) {
		bean.setOrganization(null);
		teamDAO.saveOrUpdate(bean);
	}

	@Transactional
	public void deleteTeam(Integer id) {
		Team team = teamDAO.get(id);
		if (team == null) {
			throw new UnsupportedOperationException("Nonexistent entity");
		}
		team.getTeamMembers().forEach(teamMember -> teamMemberService.deleteTeamMemberTeamConnection(teamMember, team));

		teamDAO.delete(team);
	}

	@Transactional
	public List<TeamDTO> getTeams() {
		return teamDAO.getAll().stream().map(TeamDTO::new).collect(Collectors.toList());
	}

	private void validate(TeamDTO team) throws NotificationException {
		NotificationResponse.runValidation(notificationResponse -> {
			if (StringUtils.isBlank(team.getName())) {
				notificationResponse.addEmptyFieldError(StaticMessageSource.getMessage("team.name"));
			}
			if (team.getRating() == null) {
				notificationResponse.addEmptyFieldError(StaticMessageSource.getMessage("team.rating"));
			}
		});
	}

}
