package ee.ignite.assessment.service;

import ee.ignite.assessment.dao.OrganizationDAO;
import ee.ignite.assessment.dto.OrganizationDTO;
import ee.ignite.assessment.model.Organization;
import ee.ignite.assessment.util.message.StaticMessageSource;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.util.notification.NotificationResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by mihkelk on 12.07.2017.
 */
@Service
public class OrganizationServiceImpl implements OrganizationService {

    private OrganizationDAO organizationDAO;
    private TeamService teamService;

    @Autowired
    public OrganizationServiceImpl(OrganizationDAO organizationDAO,
                                   TeamService teamService) {
        this.organizationDAO = organizationDAO;
        this.teamService = teamService;
    }

    @Transactional
    public Organization saveOrUpdate(OrganizationDTO dto) throws NotificationException {
        validate(dto);
        Organization organization = organizationDAO.getInstanceIfExistsOrNew(dto.getId());
        organization.setName(dto.getName());
        return organizationDAO.saveOrUpdate(organization);
    }

    @Transactional
    public OrganizationDTO getOrganization(Integer id) {
        Organization organization = organizationDAO.get(id);
        return new OrganizationDTO(organization);
    }

    @Transactional
    public void deleteOrganization(Integer id) {
        Organization org = organizationDAO.get(id);
        if (org == null) {
            throw new UnsupportedOperationException("Nonexistent entity");
        }
        Optional.ofNullable(org.getTeams()).ifPresent(teams -> {
            teams.forEach(team -> teamService.deleteOrganizationConnection(team));
        });
        organizationDAO.delete(org);
    }

    @Transactional
    public List<OrganizationDTO> getOrganizations() {
        return organizationDAO.getAll().stream().map(OrganizationDTO::new).collect(Collectors.toList());
    }

    private void validate(OrganizationDTO organization) throws NotificationException {
        NotificationResponse.runValidation(notificationResponse -> {
            if (StringUtils.isBlank(organization.getName())) {
                notificationResponse.addEmptyFieldError(StaticMessageSource.getMessage("org.name"));
        }
        });
    }
}
