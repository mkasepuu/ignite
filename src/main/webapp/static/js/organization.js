$(document).ready(function () {
    generateFormValidation('#organization-form', {
        name: {
            maxlength: 256,
            required: true
        }
    });
});
