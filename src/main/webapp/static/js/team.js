
$(document).ready(function () {
    generateFormValidation('#team-form', {
        name: {
            maxlength: 256,
            required: true
        },
        rating: {
            digits: true,
            range: [0, Number.MAX_SAFE_INTEGER],
            required: true
        }
    });
});
