<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:page id="organization">

    <div>
        <h1>
            <spring:message code="org.list.page"/>
            <a href="${pageContext.request.contextPath}/organization/add" class="pull-right btn btn-primary">
                <spring:message code="org.add"/>
            </a>
        </h1>
        <p>
            <spring:message code="org.list.page.info"/>
        </p>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th><spring:message code="org.id"/></th>
                <th><spring:message code="org.name"/></th>
                <th><spring:message code="org.teams"/></th>
                <th><spring:message code="org.actions"/></th>
            </tr>
            </thead>
            <tbody>

                <%--@elvariable id="organizations" type="java.util.List"--%>
            <c:forEach var="organization" items="${organizations}">
                <tr>
                    <td>${organization.id}</td>
                    <td>${organization.name}</td>
                    <td>${organization.teamNames}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/organization/edit/${organization.id}">
                            <spring:message code="global.edit"/>
                        </a>
                        <span class="separator">|</span>
                        <a href="${pageContext.request.contextPath}/organization/delete/${organization.id}">
                            <spring:message code="global.delete"/>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</t:page>