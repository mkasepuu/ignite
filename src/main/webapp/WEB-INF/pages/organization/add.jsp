<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:page id="organization">

    <script type="application/javascript"
            src="${pageContext.request.contextPath}/static/js/organization.js">
    </script>

    <form:form cssClass="form-horizontal well"
               id="organization-form"
               method="PUT"
               modelAttribute="organization"
               action="${pageContext.request.contextPath}/organization">

        <div class="text-center">
            <h1><spring:message code="org.add.page"/></h1>
            <p><spring:message code="org.add.page.info"/></p>
        </div>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">
                <spring:message code="org.name"/>:
            </label>
            <div class="col-sm-3">
                <form:input id="name" name="name" cssClass="form-control" path="name"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-3">
                <form:button class="btn btn-primary" type="submit">
                    <spring:message code="global.add"/>
                </form:button>
            </div>
        </div>
    </form:form>
</t:page>
