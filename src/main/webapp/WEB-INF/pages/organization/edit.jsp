<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:page id="organization">

    <script type="application/javascript"
            src="${pageContext.request.contextPath}/static/js/organization.js">
    </script>

    <form:form method="POST"
               modelAttribute="organization"
               id="organization-form"
               cssClass="form-horizontal well"
               action="${pageContext.request.contextPath}/organization">
        <form:hidden path="id"/>
        <div class="text-center">
            <h1><spring:message code="org.edit.page"/></h1>
            <p><spring:message code="org.edit.page.info"/></p>
        </div>
        <div class="form-group has-feedback">
            <label for="name" class="col-sm-2 control-label">
                <spring:message code="org.name"/>:
            </label>
            <div class="col-sm-3">
                <form:input name="name"
                            cssClass="form-control"
                            path="name"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-5">
                <form:button class="btn btn-primary" type="submit">
                    <spring:message code="global.edit"/>
                </form:button>
            </div>
        </div>
    </form:form>
</t:page>
