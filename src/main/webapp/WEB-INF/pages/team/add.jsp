<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:page id="team">

    <script type="application/javascript"
            src="${pageContext.request.contextPath}/static/js/team.js">
    </script>

    <form:form cssClass="form-horizontal well"
               id="team-form"
               method="PUT"
               modelAttribute="team"
               action="${pageContext.request.contextPath}/team">

        <div class="text-center">
            <h1><spring:message code="team.add.page"/></h1>
            <p><spring:message code="team.add.page.info"/></p>
        </div>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label"><spring:message code="team.name"/>:</label>
            <div class="col-sm-5">
                <form:input cssClass="form-control" path="name" name="name"/>
            </div>
        </div>
        <div class="form-group">
            <label for="rating" class="col-sm-2 control-label"><spring:message code="team.rating"/>:</label>
            <div class="col-sm-5">
                <form:input cssClass="form-control" path="rating" name="rating"/>
            </div>
        </div>
        <div class="form-group">
            <label for="organization" class="col-sm-2 control-label"><spring:message code="team.organization"/>:</label>
            <div class="col-sm-5">
                <form:select cssClass="form-control" id="organization" name="organization" path="organization.id">
                    <form:option value="" />
                    <form:options items="${organizations}" itemValue="id" itemLabel="name" />
                </form:select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-5">
                <form:button class="btn btn-primary" type="submit">
                    <spring:message code="global.add"/>
                </form:button>
            </div>
        </div>
    </form:form>
</t:page>
