<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:page id="team">
    <div>
        <h1>
            <spring:message code="team.list.page"/>
            <a href="${pageContext.request.contextPath}/team/add" class="pull-right btn btn-primary">
                <spring:message code="team.add"/>
            </a>
        </h1>
        <p>
            <spring:message code="team.list.page.info"/>
        </p>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th><spring:message code="team.id"/></th>
                <th><spring:message code="team.name"/></th>
                <th><spring:message code="team.rating"/></th>
                <th><spring:message code="team.organization"/></th>
                <th><spring:message code="team.members"/></th>
                <th><spring:message code="team.actions"/></th>
            </tr>
            </thead>
            <tbody>
                <%--@elvariable id="teams" type="java.util.List"--%>
            <c:forEach var="team" items="${teams}">
                <tr>
                    <td>${team.id}</td>
                    <td>${team.name}</td>
                    <td>${team.rating}</td>
                    <td>${team.organization.name}</td>
                    <td>${team.members}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/team/edit/${team.id}">
                            <spring:message code="global.edit"/>
                        </a>
                        <span class="separator">|</span>
                        <a href="${pageContext.request.contextPath}/team/delete/${team.id}">
                            <spring:message code="global.delete"/>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</t:page>