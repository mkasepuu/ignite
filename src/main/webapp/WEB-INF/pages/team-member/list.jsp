<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:page id="teamMember">
    <div>
        <h1>
            <spring:message code="teamMember.list.page"/>
            <a href="${pageContext.request.contextPath}/team-member/add" class="pull-right btn btn-primary">
                <spring:message code="teamMember.add"/>
            </a>
        </h1>
        <p>
            <spring:message code="teamMember.list.page.info"/>
        </p>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th><spring:message code="teamMember.id"/></th>
                <th><spring:message code="teamMember.name"/></th>
                <th><spring:message code="teamMember.teams"/></th>
                <th><spring:message code="teamMember.actions"/></th>
            </tr>
            </thead>
            <tbody>

            <%--@elvariable id="teamMembers" type="java.util.List"--%>
            <c:forEach var="teamMember" items="${teamMembers}">
                <tr>
                    <td>${teamMember.id}</td>
                    <td>${teamMember.name}</td>
                    <td>${teamMember.teamNames}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/team-member/edit/${teamMember.id}">
                            <spring:message code="global.edit"/>
                        </a>
                        <span class="separator">|</span>
                        <a href="${pageContext.request.contextPath}/team-member/delete/${teamMember.id}">
                            <spring:message code="global.delete"/>
                        </a>
                    </td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
    </div>
</t:page>