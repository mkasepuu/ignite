<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:page id="teamMember">

    <script type="application/javascript"
            src="${pageContext.request.contextPath}/static/js/team-member.js">
    </script>

    <form:form cssClass="form-horizontal well"
               method="POST"
               id="team-member-form"
               modelAttribute="teamMember"
               action="${pageContext.request.contextPath}/team-member">
        <form:hidden path="id"/>

        <div class="text-center">
            <h1><spring:message code="teamMember.edit.page"/></h1>
            <p><spring:message code="teamMember.edit.page.info"/></p>
        </div>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label"><spring:message code="teamMember.name"/>:</label>
            <div class="col-sm-5">
                <form:input cssClass="form-control" path="name" id="name" name="name"/>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label"><spring:message code="teamMember.teams"/>:</label>
            <div class="col-sm-5">
                <form:select multiple="true" cssClass="form-control" path="teamIdList">
                    <form:options items="${teams}" itemValue="id" itemLabel="name" />
                </form:select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-5">
                <form:button class="btn btn-primary" type="submit">
                    <spring:message code="global.edit"/>
                </form:button>
            </div>
        </div>
    </form:form>
</t:page>
