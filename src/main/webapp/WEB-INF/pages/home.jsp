<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:page id="home">
    <div class="text-center">
        <h1>
            <strong>
                <spring:message code="home.welcome.ignite"/>
            </strong>
        </h1>
        <p><spring:message code="home.welcome.ignite.blank"/></p>
    </div>
</t:page>