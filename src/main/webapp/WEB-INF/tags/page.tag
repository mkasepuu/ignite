<%@ tag description="Overall Page template" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="id" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/static/css/main.css"/>

    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/static/bower-components/bootstrap/dist/css/bootstrap.css"/>

    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/static/bower-components/bootstrap/dist/css/bootstrap-theme.css"/>

    <title>Ignite</title>
</head>
<body>


<script type="application/javascript"
        src="${pageContext.request.contextPath}/static/bower-components/jquery/dist/jquery.min.js"></script>
<script type="application/javascript"
        src="${pageContext.request.contextPath}/static/bower-components/jquery-validation/dist/jquery.validate.js"></script>
<script type="application/javascript"
        src="${pageContext.request.contextPath}/static/bower-components/bootstrap/dist/js/bootstrap.js"></script>
<script type="application/javascript"
        src="${pageContext.request.contextPath}/static/js/form-utlis.js"></script>

<t:navbar pageId="${id}"/>

<div class="container">
    <%--@elvariable id="notificationResponse" type="ee.ignite.assessment.util.notification.NotificationResponse"--%>
    <c:if test="${notificationResponse != null}">
        <c:forEach items="${notificationResponse.messages}" var="msg">
            <div class="alert ${msg.type.className}">
                <spring:message code="${msg.codeOrText}" text="${msg.codeOrText}"/>
            </div>
        </c:forEach>
    </c:if>

    <jsp:doBody/>

</div>

</body>
</html>