<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@tag description="Navbar template" pageEncoding="UTF-8" %>
<%@attribute name="pageId" %>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar" aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}/">
                <img src="${pageContext.request.contextPath}/static/img/logo.jpg">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li class="${pageId.equals("home") ? 'active' : ''}">
                    <a href="${pageContext.request.contextPath}/"><spring:message code="nav.home"/></a>
                </li>
                <li class="dropdown ${pageId.equals("organization") ? 'active' : ''}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <spring:message code="nav.organizations"/>&nbsp;<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/organization/add">
                                <spring:message code="global.add"/>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/organization/list">
                                <spring:message code="global.list"/>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ${pageId.equals("team") ? 'active' : ''}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false"><spring:message code="nav.teams"/>&nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/team/add">
                                <spring:message code="global.add"/>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/team/list">
                                <spring:message code="global.list"/>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ${pageId.equals("teamMember") ? 'active' : ''}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <spring:message code="nav.team.members"/>&nbsp;<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/team-member/add">
                                <spring:message code="global.add"/>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/team-member/list">
                                <spring:message code="global.list"/>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <spring:message code="nav.lang"/>&nbsp;<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="?lang=en_US">
                                <spring:message code="nav.lang.en_us"/>
                            </a>
                        </li>
                        <li>
                            <a href="?lang=en_GB">
                                <spring:message code="nav.lang.en_gb"/>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>