package ee.ignite.assessment.controller;

import ee.ignite.assessment.dto.OrganizationDTO;
import ee.ignite.assessment.init.BaseTestConfig;
import ee.ignite.assessment.init.BaseTestInitializer;
import ee.ignite.assessment.service.OrganizationService;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.util.notification.NotificationResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.FlashMap;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@ContextConfiguration(initializers = BaseTestInitializer.class, classes = BaseTestConfig.class)
public class OrganizationControllerTest {


    private static final String TEST_NAME = "TEST_NAME_";

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Mock
    private OrganizationService organizationService;

    @Autowired
    @InjectMocks
    private OrganizationController organizationController;

    @Before
    public void init() throws NotificationException {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        final List<OrganizationDTO> organizationDTOS = constructTestData();
        when(organizationService.getOrganizations()).thenReturn(organizationDTOS);
        when(organizationService.getOrganization(anyInt())).thenReturn(constructTestOrganization(-1));
        doNothing().when(organizationService).deleteOrganization(anyInt());
        when(organizationService.saveOrUpdate(any(OrganizationDTO.class))).thenReturn(null);
    }

    @Test
    public void testOrganizationListPage() throws Exception {
        mockMvc.perform(get("/organization/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("organization/list"))
                .andExpect(model().attributeExists("organizations"));
    }

    @Test
    public void testOrganizationAddPage() throws Exception {
        mockMvc.perform(get("/organization/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("organization/add"));
    }

    @Test
    public void testOrganizationEditPage() throws Exception {
        mockMvc.perform(get("/organization/edit/-1"))
                .andExpect(status().isOk())
                .andExpect(view().name("organization/edit"))
                .andExpect(model().attributeExists("organization"))
                .andExpect(model().attribute("organization", constructTestOrganization(-1)));
    }

    @Test
    public void testSaveOrUpdatePostOrganizationDTO() throws Exception {
        FlashMap organizationAddFlashMap = mockMvc.perform(post("/organization")
                .requestAttr("organization", constructTestOrganization(-1)))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/organization/list"))
                .andReturn().getFlashMap();
        assertNotNull(organizationAddFlashMap.get("notificationResponse"));
    }

    @Test
    public void testFailedSaveOrUpdatePostOrganizationDTO() throws Exception {
        when(organizationService.saveOrUpdate(any(OrganizationDTO.class))).thenThrow(constructNotificationException());

        OrganizationDTO organization = constructTestOrganization(-1);
        organization.setName(null);

        FlashMap organizationAddFlashMap = mockMvc.perform(post("/organization")
                .flashAttr("organization", organization))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/organization/edit/-1"))
                .andReturn().getFlashMap();
        assertNotNull(organizationAddFlashMap.get("organization"));
        assertNotNull(organizationAddFlashMap.get("notificationResponse"));
    }

    @Test
    public void testDelete() throws Exception {
        FlashMap organizationAddFlashMap = mockMvc.perform(get("/organization/delete/-1"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/organization/list"))
                .andReturn().getFlashMap();
        assertNotNull(organizationAddFlashMap.get("notificationResponse"));
    }

    @Test
    public void testFailedSavePostOrganizationDTO() throws Exception {
        when(organizationService.saveOrUpdate(any(OrganizationDTO.class))).thenThrow(constructNotificationException());

        OrganizationDTO organization = constructTestOrganization(null);
        organization.setName(null);

        final FlashMap organizationAddFlashMap = mockMvc.perform(put("/organization")
                .flashAttr("organization", organization))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/organization/add"))
                .andReturn().getFlashMap();
        assertNotNull(organizationAddFlashMap.get("organization"));
        assertNotNull(organizationAddFlashMap.get("notificationResponse"));
    }

    private List<OrganizationDTO> constructTestData() {
        List<OrganizationDTO> organizations = new ArrayList<>();
        organizations.add(constructTestOrganization(0));
        organizations.add(constructTestOrganization(1));
        return organizations;
    }

    private OrganizationDTO constructTestOrganization(Integer id) {
        OrganizationDTO dto = new OrganizationDTO();
        dto.setId(id);
        dto.setName(TEST_NAME + id);
        return dto;
    }

    private NotificationException constructNotificationException() {
        return new NotificationException(new NotificationResponse());
    }
}
