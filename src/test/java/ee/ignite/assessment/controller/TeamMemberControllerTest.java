package ee.ignite.assessment.controller;

import com.google.common.collect.Sets;
import ee.ignite.assessment.dto.TeamMemberDTO;
import ee.ignite.assessment.init.BaseTestConfig;
import ee.ignite.assessment.init.BaseTestInitializer;
import ee.ignite.assessment.service.TeamMemberService;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.util.notification.NotificationResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.FlashMap;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebAppConfiguration
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@ContextConfiguration(initializers = BaseTestInitializer.class, classes = BaseTestConfig.class)
public class TeamMemberControllerTest {

    private static final String TEST_NAME = "TEST_NAME_";

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Mock
    private TeamMemberService teamMemberService;

    @Autowired
    @InjectMocks
    private TeamMemberController teamMemberController;

    @Before
    public void init() throws NotificationException {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        final List<TeamMemberDTO> teamMemberDTOS = constructTestData();
        when(teamMemberService.getTeamMembers()).thenReturn(teamMemberDTOS);
        when(teamMemberService.getTeamMember(anyInt())).thenReturn(constructTestTeamMember(-1));
        doNothing().when(teamMemberService).deleteTeamMember(anyInt());
        when(teamMemberService.saveOrUpdate(any(TeamMemberDTO.class))).thenReturn(null);
    }

    @Test
    public void testTeamMemberListPage() throws Exception {
        mockMvc.perform(get("/team-member/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("team-member/list"))
                .andExpect(model().attributeExists("teamMembers"));
    }

    @Test
    public void testTeamMemberAddPage() throws Exception {
        mockMvc.perform(get("/team-member/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("team-member/add"))
                .andExpect(model().attributeExists("teamMember", "teams"));
    }

    @Test
    public void testTeamMemberEditPage() throws Exception {
        mockMvc.perform(get("/team-member/edit/-1"))
                .andExpect(status().isOk())
                .andExpect(view().name("team-member/edit"))
                .andExpect(model().attributeExists("teamMember", "teams"))
                .andExpect(model().attribute("teamMember", constructTestTeamMember(-1)));
    }

    @Test
    public void testSaveOrUpdatePostTeamMemberDTO() throws Exception {
        FlashMap teamMemberAddFlashMap = mockMvc.perform(post("/team-member")
                .requestAttr("teamMember", constructTestTeamMember(-1)))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/team-member/list"))
                .andReturn().getFlashMap();
        assertNotNull(teamMemberAddFlashMap.get("notificationResponse"));
    }

    @Test
    public void testFailedSaveOrUpdatePostTeamMemberDTO() throws Exception {
        when(teamMemberService.saveOrUpdate(any(TeamMemberDTO.class))).thenThrow(constructNotificationException());

        TeamMemberDTO teamMember = constructTestTeamMember(-1);
        teamMember.setName(null);

        FlashMap teamMemberAddFlashMap = mockMvc.perform(post("/team-member")
                .flashAttr("teamMember", teamMember))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/team-member/edit/-1"))
                .andReturn().getFlashMap();
        assertNotNull(teamMemberAddFlashMap.get("teamMember"));
        assertNotNull(teamMemberAddFlashMap.get("notificationResponse"));
    }

    @Test
    public void testFailedSavePostTeamMemberDTO() throws Exception {
        when(teamMemberService.saveOrUpdate(any(TeamMemberDTO.class))).thenThrow(constructNotificationException());

        TeamMemberDTO teamMember = constructTestTeamMember(null);
        teamMember.setName(null);

        FlashMap teamMemberAddFlashMap = mockMvc.perform(put("/team-member")
                .flashAttr("teamMember", teamMember))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/team-member/add"))
                .andReturn().getFlashMap();
        assertNotNull(teamMemberAddFlashMap.get("teamMember"));
        assertNotNull(teamMemberAddFlashMap.get("notificationResponse"));
    }

    @Test
    public void testDelete() throws Exception {
        FlashMap organizationAddFlashMap = mockMvc.perform(get("/team-member/delete/-1"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/team-member/list"))
                .andReturn().getFlashMap();
        assertNotNull(organizationAddFlashMap.get("notificationResponse"));
    }

    private List<TeamMemberDTO> constructTestData() {
        List<TeamMemberDTO> teamMembers = new ArrayList<>();
        teamMembers.add(constructTestTeamMember(0));
        teamMembers.add(constructTestTeamMember(1));
        return teamMembers;
    }

    private TeamMemberDTO constructTestTeamMember(Integer id) {
        TeamMemberDTO dto = new TeamMemberDTO();
        dto.setId(id);
        dto.setTeamNames("A, B");
        dto.setTeamIdList(Sets.newHashSet(0, 1));
        dto.setName(TEST_NAME + id);
        return dto;
    }

    private NotificationException constructNotificationException() {
        return new NotificationException(new NotificationResponse());
    }
}
