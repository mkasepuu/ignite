package ee.ignite.assessment.controller;

import ee.ignite.assessment.dto.TeamDTO;
import ee.ignite.assessment.init.BaseTestConfig;
import ee.ignite.assessment.init.BaseTestInitializer;
import ee.ignite.assessment.service.OrganizationService;
import ee.ignite.assessment.service.TeamService;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.util.notification.NotificationResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.FlashMap;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@ContextConfiguration(initializers = BaseTestInitializer.class, classes = BaseTestConfig.class)
public class TeamControllerTest {


    private static final String TEST_NAME = "TEST_NAME_";

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Mock
    private TeamService teamService;

    @Mock
    private OrganizationService organizationService;

    @Autowired
    @InjectMocks
    private TeamController teamController;

    @Before
    public void init() throws NotificationException {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        final List<TeamDTO> teamDTOS = constructTestData();
        when(teamService.getTeams()).thenReturn(teamDTOS);
        when(organizationService.getOrganizations()).thenReturn(new ArrayList<>());
        when(teamService.getTeam(anyInt())).thenReturn(constructTestTeam(-1));
        doNothing().when(teamService).deleteTeam(anyInt());
        when(teamService.saveOrUpdate(any(TeamDTO.class))).thenReturn(null);
    }

    @Test
    public void testTeamListPage() throws Exception {
        mockMvc.perform(get("/team/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("team/list"))
                .andExpect(model().attributeExists("teams"));
    }

    @Test
    public void testTeamAddPage() throws Exception {
        mockMvc.perform(get("/team/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("team/add"))
                .andExpect(model().attributeExists("team", "organizations"));
    }

    @Test
    public void testTeamEditPage() throws Exception {
        mockMvc.perform(get("/team/edit/-1"))
                .andExpect(status().isOk())
                .andExpect(view().name("team/edit"))
                .andExpect(model().attributeExists("team", "team"))
                .andExpect(model().attribute("team", constructTestTeam(-1)));
    }

    @Test
    public void testSaveOrUpdatePostTeamDTO() throws Exception {
        FlashMap teamAddFlashMap = mockMvc.perform(post("/team")
                .requestAttr("team", constructTestTeam(-1)))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/team/list"))
                .andReturn().getFlashMap();
        assertNotNull(teamAddFlashMap.get("notificationResponse"));
    }

    @Test
    public void testFailedSaveOrUpdatePostTeamDTO() throws Exception {
        when(teamService.saveOrUpdate(any(TeamDTO.class))).thenThrow(constructNotificationException());

        TeamDTO team = constructTestTeam(-1);
        team.setName(null);

        FlashMap teamAddFlashMap = mockMvc.perform(post("/team")
                .flashAttr("team", team))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/team/edit/-1"))
                .andReturn().getFlashMap();
        assertNotNull(teamAddFlashMap.get("team"));
        assertNotNull(teamAddFlashMap.get("notificationResponse"));
    }

    @Test
    public void testFailedSavePostTeamDTO() throws Exception {
        when(teamService.saveOrUpdate(any(TeamDTO.class))).thenThrow(constructNotificationException());

        TeamDTO team = constructTestTeam(null);
        team.setName(null);

        FlashMap teamAddFlashMap = mockMvc.perform(put("/team")
                .flashAttr("team", team))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/team/add"))
                .andReturn().getFlashMap();
        assertNotNull(teamAddFlashMap.get("team"));
        assertNotNull(teamAddFlashMap.get("notificationResponse"));
    }

    @Test
    public void testDelete() throws Exception {
        FlashMap organizationAddFlashMap = mockMvc.perform(get("/team/delete/-1"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/team/list"))
                .andReturn().getFlashMap();
        assertNotNull(organizationAddFlashMap.get("notificationResponse"));
    }

    private List<TeamDTO> constructTestData() {
        List<TeamDTO> teams = new ArrayList<>();
        teams.add(constructTestTeam(0));
        teams.add(constructTestTeam(1));
        return teams;
    }

    private TeamDTO constructTestTeam(Integer id) {
        TeamDTO dto = new TeamDTO();
        dto.setId(id);
        dto.setRating(-12);
        dto.setName(TEST_NAME + id);
        return dto;
    }
    
    private NotificationException constructNotificationException() {
        return new NotificationException(new NotificationResponse());
    }
}
