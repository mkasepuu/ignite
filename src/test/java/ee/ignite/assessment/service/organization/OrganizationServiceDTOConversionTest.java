package ee.ignite.assessment.service.organization;

import ee.ignite.assessment.dao.OrganizationDAO;
import ee.ignite.assessment.dto.OrganizationDTO;
import ee.ignite.assessment.init.BaseTestConfig;
import ee.ignite.assessment.init.BaseTestInitializer;
import ee.ignite.assessment.model.Organization;
import ee.ignite.assessment.service.OrganizationService;
import ee.ignite.assessment.service.TeamService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@WebAppConfiguration
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@ContextConfiguration(initializers = BaseTestInitializer.class, classes = BaseTestConfig.class)
public class OrganizationServiceDTOConversionTest {

    private static final String TEST_NAME = "Test org";
    private static final String TEST_TEAM_NAMES = "Random team names";

    @Autowired
    private OrganizationService organizationService;

    @Mock
    private OrganizationDAO organizationDAO;

    @Mock
    private TeamService teamService;

    @Captor
    private ArgumentCaptor<Organization> argumentCaptor;

    private OrganizationDTO DTO;

    @Before
    public void init() {
        ReflectionTestUtils.setField(organizationService, "organizationDAO", organizationDAO);
        ReflectionTestUtils.setField(organizationService, "teamService", teamService);
        DTO = new OrganizationDTO();
        DTO.setName(TEST_NAME);
        DTO.setTeamNames(TEST_TEAM_NAMES);
    }

    @Test
    public void testDtoToModelConversion() throws Exception {
        when(organizationDAO.getInstanceIfExistsOrNew(null)).thenReturn(new Organization());
        when(organizationDAO.saveOrUpdate(argumentCaptor.capture())).thenReturn(new Organization());

        organizationService.saveOrUpdate(DTO);
        Organization actualValue = argumentCaptor.getValue();
        assertEquals(TEST_NAME, actualValue.getName());
    }
}
