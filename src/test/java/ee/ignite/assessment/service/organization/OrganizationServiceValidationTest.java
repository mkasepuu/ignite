package ee.ignite.assessment.service.organization;

import ee.ignite.assessment.dto.OrganizationDTO;
import ee.ignite.assessment.init.BaseTestInitializer;
import ee.ignite.assessment.service.OrganizationService;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.init.BaseTestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(initializers = BaseTestInitializer.class, classes = BaseTestConfig.class)
public class OrganizationServiceValidationTest {

    @Autowired
    private OrganizationService organizationService;


    @Test(expected = NotificationException.class)
    public void testNoNameValidation() throws Exception {
        OrganizationDTO organizationDTO = new OrganizationDTO();
        organizationService.saveOrUpdate(organizationDTO);
    }



}
