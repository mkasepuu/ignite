package ee.ignite.assessment.service.member;

import com.google.common.collect.Sets;
import ee.ignite.assessment.dto.TeamDTO;
import ee.ignite.assessment.dto.TeamMemberDTO;
import ee.ignite.assessment.service.TeamMemberService;
import ee.ignite.assessment.service.TeamService;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.init.BaseTestConfig;
import ee.ignite.assessment.init.BaseTestInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@Rollback
@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(initializers = BaseTestInitializer.class, classes = BaseTestConfig.class)
public class TeamMemberServiceValidationTest {

    @Autowired
    private TeamMemberService teamMemberService;

    @Autowired
    private TeamService teamService;

    @Test(expected = NotificationException.class)
    public void testNoNameValidation() throws Exception {
        TeamMemberDTO teamMemberDTO = new TeamMemberDTO();
        teamMemberService.saveOrUpdate(teamMemberDTO);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testNonExistentOrgValidation() throws Exception {
        TeamMemberDTO teamMemberDTO = new TeamMemberDTO();
        teamMemberDTO.setTeamIdList(Sets.newHashSet(getNonExistentTeamDTO().getId()));
        teamMemberDTO.setName("Test");
        teamMemberService.saveOrUpdate(teamMemberDTO);
    }

    private TeamDTO getNonExistentTeamDTO() {
        Integer id = 0;
        TeamDTO dto = null;
        while (dto == null) {
            try {
                teamService.getTeam(--id);
            } catch (Exception e) {
                dto = new TeamDTO();
                dto.setId(id);
                dto.setName("Random name");
            }
        }
        return dto;
    }
}
