package ee.ignite.assessment.service.member;

import com.google.common.collect.Sets;
import ee.ignite.assessment.dao.TeamDAO;
import ee.ignite.assessment.dao.TeamMemberDAO;
import ee.ignite.assessment.dto.TeamMemberDTO;
import ee.ignite.assessment.init.BaseTestConfig;
import ee.ignite.assessment.init.BaseTestInitializer;
import ee.ignite.assessment.model.Team;
import ee.ignite.assessment.model.TeamMember;
import ee.ignite.assessment.service.TeamMemberService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.when;

@WebAppConfiguration
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@ContextConfiguration(initializers = BaseTestInitializer.class, classes = BaseTestConfig.class)
public class TeamMemberServiceDTOConversionTest {

    private static final String TEST_NAME = "Test org";
    private static final String TEST_TEAM_NAME = "TEAM_NAME_";

    @Autowired
    private TeamMemberService teamMemberService;

    @Mock
    private TeamMemberDAO teamMemberDAO;

    @Mock
    private TeamDAO teamDAO;

    @Captor
    private ArgumentCaptor<TeamMember> argumentCaptor;

    private TeamMemberDTO DTO;

    @Before
    public void init() {
        ReflectionTestUtils.setField(teamMemberService, "teamMemberDAO", teamMemberDAO);
        ReflectionTestUtils.setField(teamMemberService, "teamDAO", teamDAO);
        DTO = new TeamMemberDTO();
        DTO.setName(TEST_NAME);
        DTO.setTeamIdList(Sets.newHashSet(0, 1));
        DTO.setTeamNames("A, B");
    }

    @Test
    public void testDtoToModelConversion() throws Exception {

        when(teamMemberDAO.getInstanceIfExistsOrNew(null)).thenReturn(new TeamMember());
        when(teamDAO.get(0)).thenReturn(generateTestTeam(0));
        when(teamDAO.get(1)).thenReturn(generateTestTeam(1));
        when(teamMemberDAO.saveOrUpdate(argumentCaptor.capture())).thenReturn(new TeamMember());

        teamMemberService.saveOrUpdate(DTO);
        TeamMember actualValue = argumentCaptor.getValue();

        assertEquals(TEST_NAME, actualValue.getName());
        assertNotNull(actualValue.getTeams());
        actualValue.getTeams().forEach(
                team -> assertTrue(teamAssertion(0, team) || teamAssertion(1, team))
        );
    }

    private boolean teamAssertion(Integer id, Team team) {
        return id.equals(team.getId()) && (TEST_TEAM_NAME + id).equals(team.getName());
    }

    private Team generateTestTeam(Integer id) {
        Team team = new Team();
        team.setId(id);
        team.setName(TEST_TEAM_NAME + id);
        return team;
    }
}
