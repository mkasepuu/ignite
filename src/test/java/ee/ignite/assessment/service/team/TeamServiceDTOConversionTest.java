package ee.ignite.assessment.service.team;

import ee.ignite.assessment.dao.OrganizationDAO;
import ee.ignite.assessment.dao.TeamDAO;
import ee.ignite.assessment.dto.OrganizationDTO;
import ee.ignite.assessment.dto.TeamDTO;
import ee.ignite.assessment.init.BaseTestConfig;
import ee.ignite.assessment.init.BaseTestInitializer;
import ee.ignite.assessment.model.Organization;
import ee.ignite.assessment.model.Team;
import ee.ignite.assessment.service.TeamMemberService;
import ee.ignite.assessment.service.TeamService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@WebAppConfiguration
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@ContextConfiguration(initializers = BaseTestInitializer.class, classes = BaseTestConfig.class)
public class TeamServiceDTOConversionTest {

    private static final String TEST_NAME = "Test org";
    private static final String TEST_TEAM_NAMES = "Random team-member names";
    private static final Integer TEST_RATING = 12;
    private static final Integer TEST_ORG_ID = -1;
    private static final String TEST_ORG_NAME = "ORG_NAME";

    @Autowired
    private TeamService teamService;

    @Mock
    private TeamDAO teamDAO;

    @Mock
    private OrganizationDAO organizationDAO;

    @Mock
    private TeamMemberService teamMemberService;

    @Captor
    private ArgumentCaptor<Team> argumentCaptor;

    private TeamDTO DTO;

    @Before
    public void init() {
        ReflectionTestUtils.setField(teamService, "teamDAO", teamDAO);
        ReflectionTestUtils.setField(teamService, "organizationDAO", organizationDAO);
        ReflectionTestUtils.setField(teamService, "teamMemberService", teamMemberService);
        DTO = new TeamDTO();
        DTO.setName(TEST_NAME);
        DTO.setRating(TEST_RATING);
        OrganizationDTO organizationDTO = new OrganizationDTO();
        organizationDTO.setId(TEST_ORG_ID);
        DTO.setOrganization(organizationDTO);
        DTO.setMembers(TEST_TEAM_NAMES);
    }



    @Test
    public void testDtoToModelConversion() throws Exception {

        when(teamDAO.getInstanceIfExistsOrNew(null)).thenReturn(new Team());
        when(organizationDAO.get(TEST_ORG_ID)).thenReturn(generateTestOrganisation());
        when(teamDAO.saveOrUpdate(argumentCaptor.capture())).thenReturn(new Team());

        teamService.saveOrUpdate(DTO);
        Team actualValue = argumentCaptor.getValue();

        assertEquals(TEST_NAME, actualValue.getName());
        assertEquals(TEST_RATING, actualValue.getRating());
        assertEquals(TEST_ORG_ID, actualValue.getOrganization().getId());
        assertEquals(TEST_ORG_NAME, actualValue.getOrganization().getName());
    }

    private Organization generateTestOrganisation() {
        Organization org = new Organization();
        org.setId(TEST_ORG_ID);
        org.setName(TEST_ORG_NAME);
        return org;
    }
}
