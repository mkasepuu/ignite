package ee.ignite.assessment.service.team;

import ee.ignite.assessment.dto.OrganizationDTO;
import ee.ignite.assessment.dto.TeamDTO;
import ee.ignite.assessment.service.OrganizationService;
import ee.ignite.assessment.service.TeamService;
import ee.ignite.assessment.util.notification.NotificationException;
import ee.ignite.assessment.init.BaseTestConfig;
import ee.ignite.assessment.init.BaseTestInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@Rollback
@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(initializers = BaseTestInitializer.class, classes = BaseTestConfig.class)
public class TeamServiceValidationTest {

    @Autowired
    private TeamService teamService;

    @Autowired
    private OrganizationService organizationService;

    @Test(expected = NotificationException.class)
    public void testNoNameValidation() throws Exception {
        TeamDTO teamDTO = new TeamDTO();
        teamDTO.setRating(12);
        teamService.saveOrUpdate(teamDTO);
    }

    @Test(expected = NotificationException.class)
    public void testNoRatingValidation() throws Exception {
        TeamDTO teamDTO = new TeamDTO();
        teamDTO.setName("A");
        teamService.saveOrUpdate(teamDTO);
    }

    @Test(expected = NotificationException.class)
    public void testNoNameNoRatingValidation() throws Exception {
        TeamDTO teamDTO = new TeamDTO();
        teamService.saveOrUpdate(teamDTO);
    }

    @Test
    public void testValidNameAndRatingValidation() throws Exception {
        TeamDTO teamDTO = new TeamDTO();
        teamDTO.setName("A");
        teamDTO.setRating(123);
        teamService.saveOrUpdate(teamDTO);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testNonExistentOrgValidation() throws Exception {
        TeamDTO teamDTO = new TeamDTO();
        teamDTO.setOrganization(getNonExistentOrgDTO());
        teamDTO.setRating(0);
        teamDTO.setName("Test");
        teamService.saveOrUpdate(teamDTO);
    }

    private OrganizationDTO getNonExistentOrgDTO() {
        Integer id = 0;
        OrganizationDTO dto = null;
        while (dto == null) {
            try {
                organizationService.getOrganization(--id);
            } catch (Exception e) {
                dto = new OrganizationDTO();
                dto.setId(id);
                dto.setName("Random name");
            }
        }
        return dto;
    }
}
