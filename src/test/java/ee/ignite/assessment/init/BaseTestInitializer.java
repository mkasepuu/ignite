package ee.ignite.assessment.init;

import ee.ignite.assessment.init.AppParamInitializer;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;


public class BaseTestInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {
		AppParamInitializer.initSystemParams();
	}
}
