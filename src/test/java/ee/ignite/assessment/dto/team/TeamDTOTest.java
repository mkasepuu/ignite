package ee.ignite.assessment.dto.team;

import com.google.common.collect.Sets;
import ee.ignite.assessment.dto.TeamDTO;
import ee.ignite.assessment.model.Team;
import ee.ignite.assessment.model.TeamMember;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class TeamDTOTest {

    private static final String TEST_NAME = "Test Team";

    private Team model;
    private TeamDTO DTO;

    @Before
    public void init() {
        Team fullModel = new Team();
        fullModel.setId(-1);
        fullModel.setName(TEST_NAME);
        fullModel.setTeamMembers(Sets.newHashSet());
        for (int id = 0; id < 2; id++) {
            fullModel.getTeamMembers().add(createTeamMember(id));
        }
        DTO = new TeamDTO(fullModel);
        model = fullModel;

    }

    @Test
    public void testIdConversion() {
        assertEquals(model.getId(), DTO.getId());
    }

    @Test
    public void testNameConversion() {
        assertEquals(model.getName(), DTO.getName());
    }

    @Test
    public void testMultipleTeamMemberNamesConversion() {
        assertTrue("0, 1".equals(DTO.getMembers()) || "1, 0".equals(DTO.getMembers()));
    }

    @Test
    public void testSingleTeamNameConversion() {
        model.setTeamMembers(Sets.newHashSet(createTeamMember(0)));
        assertEquals("0", new TeamDTO(model).getMembers());
    }

    private TeamMember createTeamMember(Integer id) {
        TeamMember teamMember = new TeamMember();
        teamMember.setId(-id);
        teamMember.setName(String.valueOf(id));
        return teamMember;
    }
}
