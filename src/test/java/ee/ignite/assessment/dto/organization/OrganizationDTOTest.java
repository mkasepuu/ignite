package ee.ignite.assessment.dto.organization;

import com.google.common.collect.Sets;
import ee.ignite.assessment.dto.OrganizationDTO;
import ee.ignite.assessment.model.Organization;
import ee.ignite.assessment.model.Team;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class OrganizationDTOTest {

    private static final String TEST_NAME = "Test Org";

    private Organization model;
    private OrganizationDTO DTO;

    @Before
    public void init() {
        Organization fullModel = new Organization();
        fullModel.setId(-1);
        fullModel.setName(TEST_NAME);
        fullModel.setTeams(Sets.newHashSet());
        for (int id = 0; id < 2; id++) {
            fullModel.getTeams().add(createTeam(id));
        }
        DTO = new OrganizationDTO(fullModel);
        model = fullModel;

    }
    @Test
    public void testIdConversion() {
        assertEquals(model.getId(), DTO.getId());
    }

    @Test
    public void testNameConversion() {
        assertEquals(model.getName(), DTO.getName());
    }

    @Test
    public void testMultipleTeamNamesConversion() {
        assertTrue("0, 1".equals(DTO.getTeamNames()) || "1, 0".equals(DTO.getTeamNames()));
    }

    @Test
    public void testSingleTeamNameConversion() {
        model.setTeams(Sets.newHashSet(createTeam(0)));
        assertEquals("0", new OrganizationDTO(model).getTeamNames());
    }

    private Team createTeam(Integer id) {
        Team team = new Team();
        team.setId(-id);
        team.setName(String.valueOf(id));
        return team;
    }

}
