package ee.ignite.assessment.dto.member;

import com.google.common.collect.Sets;
import ee.ignite.assessment.dto.TeamMemberDTO;
import ee.ignite.assessment.model.Team;
import ee.ignite.assessment.model.TeamMember;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class TeamMemberDTOTest {

    private static final String TEST_NAME = "Test Team Member";

    private TeamMember model;
    private TeamMemberDTO DTO;

    @Before
    public void init() {
        TeamMember fullModel = new TeamMember();
        fullModel.setId(-1);
        fullModel.setName(TEST_NAME);
        fullModel.setTeams(Sets.newHashSet());
        for (int id = 0; id < 2; id++) {
            fullModel.getTeams().add(createTeam(id));
        }
        DTO = new TeamMemberDTO(fullModel);
        model = fullModel;
    }

    @Test
    public void testIdConversion() {
        assertEquals(model.getId(), DTO.getId());
    }

    @Test
    public void testNameConversion() {
        assertEquals(model.getName(), DTO.getName());
    }

    @Test
    public void testMultipleTeamNamesConversion() {
        assertTrue("0, 1".equals(DTO.getTeamNames()) || "1, 0".equals(DTO.getTeamNames()));
    }

    @Test
    public void testSingleTeamNameConversion() {
        model.setTeams(Sets.newHashSet(createTeam(0)));
        assertEquals("0", new TeamMemberDTO(model).getTeamNames());
    }

    @Test
    public void testTeamIdListConversion() {
        HashSet<Integer> integerHashSet = Sets.newHashSet(0, -1);
        assertEquals(integerHashSet, Sets.newHashSet(DTO.getTeamIdList()));
    }

    private Team createTeam(Integer id) {
        Team team = new Team();
        team.setId(-id);
        team.setName(String.valueOf(id));
        return team;
    }
}
